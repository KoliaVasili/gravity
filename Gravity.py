import pygame
from random import randint, choice
from math import pi, sin, cos, atan2

class SpaceObject:
    def __init__(self, pos, speed, angle, color, shape):

        self.pos = pygame.math.Vector2(pos)
        self.speed = speed
        self.angle = angle*(pi/180) #convetrs degrees to radians
        self.color = color
        self.shape = shape
        self.image = pygame.Surface((30, 30), pygame.SRCALPHA)
        self.construct_obj()

    def construct_obj(self):
        if self.shape == 'square':
            self.image.fill(self.color)
        elif self.shape == 'circle':
            pygame.draw.circle(self.image, self.color, (15, 15), 15)
        elif self.shape == 'triangle':
            pygame.draw.polygon(self.image, self.color, ([15, 0], [30, 30], [0, 30]))
        elif self.shape == 'diamond':
            pygame.draw.polygon(self.image, self.color, ([15, 0], [30, 15], [15, 30], [0, 15]))

    def run(self):

        if gravity_bool:
            g = (gravity_angle-90)*pi/180
            self.pos.x += (cos(g) *5)
            self.pos.y += (sin(g) *5)

        self.pos.x += (self.speed*cos(self.angle))
        self.pos.y += (self.speed*sin(self.angle))

        #teleports to the other side of screen when they hit an edge        
        if self.pos.x > W:
            self.pos.x = 0
        elif self.pos.x < 0:
            self.pos.x = W
        if self.pos.y > H:
            self.pos.y = 0
        elif self.pos.y < 0:
            self.pos.y = H

#--------------Initializing-----------------

pygame.init()
W = H = 700
# screen = pygame.display.set_mode((W, H))
screen = pygame.display.set_mode((W, H), pygame.FULLSCREEN | pygame.SCALED)
clock = pygame.time.Clock()
pygame.display.set_caption('Gravity SIM')
font = pygame.font.Font(None, 35)

# -------------Angle Control UI--------------

angle_ui = pygame.Surface((400, 400), pygame.SRCALPHA)
angle_pointer = pygame.Surface((40, 40), pygame.SRCALPHA)
pygame.draw.circle(angle_ui, [100]*4, (200, 200), 200, 5)
pygame.draw.circle(angle_pointer, [100]*4, (20, 20), 20)
angle_ui_red = pygame.Surface((400, 400), pygame.SRCALPHA)
angle_pointer_red = pygame.Surface((40, 40), pygame.SRCALPHA)
pygame.draw.circle(angle_ui_red, (200, 60, 12), (200, 200), 200, 5)
pygame.draw.circle(angle_pointer_red, (200, 60, 12), (20, 20), 20)

gravity_toggle_surf = pygame.Surface((30, 30), pygame.SRCALPHA)
pygame.draw.rect(gravity_toggle_surf, ('grey28'), (0, 0, 30, 30), 3)
gravity_toggle_rect = gravity_toggle_surf.get_rect(topleft = (20, 20))

quit_surf = pygame.Surface((30, 30), pygame.SRCALPHA)
q_surf = font.render('Q', True, 'grey28')
q_rect = q_surf.get_rect(center=(15, 15))
quit_surf.blit(q_surf, q_rect)
pygame.draw.rect(quit_surf, ('grey28'), (0, 0, 30, 30), 3)
quit_rect = quit_surf.get_rect(topright = (W-20, 20))

#----------------Variables------------------

COLOURS = [c for c in  pygame.colordict.THECOLORS.keys() if not ('grey' in c or 'gray' in c)] # list of colours excluding greys
gravity_bool = False
gravity_angle = 0
gravity_angle_rad = 0
objects = []
number_of_obj = 50

#----------Creating Objects---------------

for _ in range(number_of_obj):

    obj = SpaceObject(
        [randint(10, W-10), randint(10, H-10)], #pos
        randint(1, 5), #speed
        randint(0, 360), #angle
        choice(COLOURS), #color
        choice(['square', 'circle', 'triangle', 'diamond'])) #shape
    objects.append(obj)

#----------------Main Loop----------------

while True:

    pygame.display.update()
    clock.tick(60)

    mouse = pygame.mouse.get_pressed()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            raise SystemExit
        if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
            gravity_bool = not gravity_bool
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button==1:
            if gravity_toggle_rect.collidepoint(event.pos):
                gravity_bool = not gravity_bool
            elif quit_rect.collidepoint(event.pos):
                pygame.quit()
                raise SystemExit
        if event.type == pygame.MOUSEMOTION or event.type == pygame.FINGERMOTION:
            change_angle = True
        else:
            change_angle = False

    screen.fill('grey80')

    for obj in objects:

        #drawing objs to screen
        obj.run()
        screen.blit(obj.image, [int(obj.pos.x),int(obj.pos.y)])

    screen.blit(gravity_toggle_surf, gravity_toggle_rect)
    screen.blit(quit_surf, quit_rect)
    if not gravity_bool:
        pygame.draw.rect(screen, (200, 60, 12), (20, 20, 30, 30), 3)

    if mouse[0] and not gravity_toggle_rect.collidepoint(pygame.mouse.get_pos()):


        if change_angle:
            mosx, mosy = pygame.mouse.get_pos()
            gravity_angle_rad = atan2( (mosy)-(H//2), (mosx) - (W//2) ) #atan of slop of line
            gravity_angle = gravity_angle_rad *180/pi +90

        col = [100]*4 if gravity_bool else (200, 60, 12)
        angle_ui_done = angle_ui if gravity_bool else angle_ui_red
        angle_pointer_done = angle_pointer if gravity_bool else angle_pointer_red
        p_pos = cos(gravity_angle_rad)*197, sin(gravity_angle_rad)*197
        # print(p_pos)

        screen.blit(angle_ui_done, (W//2-200,H//2-200))
        screen.blit(angle_pointer_done, (p_pos[0]+(W//2-20), p_pos[1]+(H//2-20)))

        angle = gravity_angle+360 if gravity_angle and abs(gravity_angle)/gravity_angle == -1 else gravity_angle

        text = font.render(f'{int(angle)}°', True, col)
        text_rect = text.get_rect(center = (W//2,H//2))
        screen.blit(text, text_rect)
